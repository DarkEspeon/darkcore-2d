package com.DarkEG.Engine.Process;

public abstract class DarkProcess {
	private ProcessState state = ProcessState.UNINITIALIZED;
	private DarkProcess child = null;
	
	protected abstract void onInit();
	public abstract void onUpdate(float delta);
	public void onSuccess(){}
	public void onFail(){}
	public void onAbort(){}
	
	public void Init(){
		state = ProcessState.RUNNING;
		onInit();
	}
	
	public void Succeed(){ state = ProcessState.SUCCEEDED; }
	public void Fail(){ state = ProcessState.FAILED; }
	
	public void Pause(){ state = ProcessState.PAUSED; }
	public void UnPause(){ state = ProcessState.RUNNING; }
	
	public ProcessState getState(){ return state; }
	public boolean isAlive(){ return state == ProcessState.RUNNING || state == ProcessState.PAUSED; }
	public boolean isDead(){ return state == ProcessState.SUCCEEDED || state == ProcessState.FAILED || state == ProcessState.ABORTED; }
	public boolean isRemoved(){ return state == ProcessState.REMOVED; }
	public boolean isPaused(){ return state == ProcessState.PAUSED; }
	
	public void attachChild(DarkProcess child){ this.child = child; }
	public DarkProcess removeChild(){ 
		DarkProcess childPro = child;
		child = null;
		return childPro; 
	}
	public DarkProcess peekChild(){ return child; }
}
