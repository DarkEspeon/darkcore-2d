package com.DarkEG.Engine.Process;

public enum ProcessState {
	// NOT DEAD OR ALIVE
	UNINITIALIZED,
	REMOVED,
	// ALIVE
	RUNNING,
	PAUSED,
	//DEAD
	SUCCEEDED,
	FAILED,
	ABORTED;
}
