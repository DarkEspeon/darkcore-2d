package com.DarkEG.Engine.Process.Default;

import java.util.List;

import com.DarkEG.Engine.Entity.EntitySystem;
import com.DarkEG.Engine.Entity.System.ComponentSystem;
import com.DarkEG.Engine.Process.DarkProcess;

public class ComponentSystemProcess extends DarkProcess {
	private EntitySystem es;
	protected void onInit() {
		this.es = EntitySystem.getSystem();
	}
	public void onUpdate(float delta) {
		List<ComponentSystem> system = es.getComponentSystems();
		for(ComponentSystem cs : system){
			cs.process(delta);
		}
	}
	
}
