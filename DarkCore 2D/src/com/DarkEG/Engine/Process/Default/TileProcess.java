package com.DarkEG.Engine.Process.Default;

import com.DarkEG.Engine.Level.Tiles.Tile;
import com.DarkEG.Engine.Process.DarkProcess;

public class TileProcess extends DarkProcess {
	protected void onInit() {}
	public void onUpdate(float delta) {
		for(Tile tile : Tile.tiles){
			if(tile == null) continue;
			else tile.tick();
		}
	}

}
