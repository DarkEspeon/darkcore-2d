package com.DarkEG.Engine.Process;

import java.util.ArrayList;
import java.util.List;

public class ProcessManager {
	List<DarkProcess> processes = new ArrayList<>();
	private boolean abortAll = false;
	public ProcessManager(){}
	public void updateProcesses(float deltaMs){
		List<DarkProcess> addedProcessList = new ArrayList<>();
		List<DarkProcess> removeProcessList = new ArrayList<>();
		for(DarkProcess p : processes){
			if(p == null) continue;
			if(abortAll) {
				p.onAbort();
				continue;
			} else {
				if(p.getState() == ProcessState.UNINITIALIZED){
					p.Init();
				}
				if(p.getState() == ProcessState.RUNNING){
					p.onUpdate(deltaMs);
				}
				if(p.isDead()){
					switch(p.getState()){
					case SUCCEEDED:
						p.onSuccess();
						DarkProcess pChild = p.removeChild();
						addedProcessList.add(pChild);
						break;
					case FAILED:
						p.onFail();
						break;
					case ABORTED:
						p.onAbort();
						break;
					}
					removeProcessList.add(p);
				}
			}
		}
		for(DarkProcess p : addedProcessList){
			processes.add(p);
		}
		for(DarkProcess p : removeProcessList){
			processes.remove(p);
		}
		addedProcessList.clear();
		removeProcessList.clear();
	}
	public void abortAll(boolean immediate){
		if(immediate){
			for(DarkProcess p : processes){
				p.Fail();
			}
		} else abortAll = true;
	}
	public void attachProcess(DarkProcess p){ processes.add(p); }
	public int getProcessNum(){ return processes.size(); }
	public void cleanUp(){
		abortAll(true);
		processes.clear();
	}
}
