package com.DarkEG.Engine.Level;

import com.google.gson.annotations.Expose;

public class LevelConnection {
	@Expose private String level;
	@Expose private int fromX, fromY;
	@Expose private int toX, toY;
	public LevelConnection(String level, int fromX, int fromY, int toX, int toY){
		this.level = level;
		this.fromX = fromX;
		this.fromY = fromY;
		this.toX = toX;
		this.toY = toY;
	}
	public String getLevel(){ return level; }
	public int getFromX(){ return fromX; }
	public int getFromY(){ return fromY; }
	public int getToX(){ return toX; }
	public int getToY(){ return toY; }
	public boolean isPositionOnConnection(int x, int y){ return x == fromX && y == fromY; }
}
