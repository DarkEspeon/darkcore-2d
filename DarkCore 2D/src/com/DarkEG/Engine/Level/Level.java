package com.DarkEG.Engine.Level;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.DarkEG.Engine.Asset.AssetLoader;
import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.LevelLoadedEvent;
import com.DarkEG.Engine.Event.Default.LevelTransitionEvent;
import com.DarkEG.Engine.Event.Default.SpriteSwitchEvent;
import com.DarkEG.Engine.Event.Default.XOffsetEvent;
import com.DarkEG.Engine.Event.Default.YOffsetEvent;
import com.DarkEG.Engine.Level.Tiles.Tile;
import com.DarkEG.Engine.Render.SpriteSheet;
import com.DarkEG.Engine.Util.EventID;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

public class Level implements EventListener {
	public static boolean firstLoad = false;
	
	@Expose private String name, readableName;
	@Expose private int width, height;
	private int xOffset, yOffset;
	@Expose private SpriteSheet sheet;
	@Expose private String tileMap;
	private byte[] tiles;
	@Expose private List<LevelConnection> connections = new ArrayList<>();
	
	public Level(){
		EventCore.getBus().addListener(this, EventID.XRENDEROFFSET);
		EventCore.getBus().addListener(this, EventID.YRENDEROFFSET);
	}
	public Level(String levelName, String readableName, String tileMap, int width, int height, SpriteSheet sheet, byte[] tiles){
		this.name = levelName;
		this.readableName = readableName;
		this.tileMap = tileMap;
		this.width = width;
		this.height = height;
		this.tiles = new byte[tiles.length];
		this.sheet = sheet;
		System.arraycopy(tiles, 0, this.tiles, 0, tiles.length);
		EventCore.getBus().addListener(this, EventID.XRENDEROFFSET);
		EventCore.getBus().addListener(this, EventID.YRENDEROFFSET);
	}
	public void renderLevel(int width, int height){
		int yMin = yOffset / sheet.spriteSize;
		int xMin = xOffset / sheet.spriteSize;
		int yMax = (yOffset + height) / sheet.spriteSize + 1;
		int xMax = (xOffset + width) / sheet.spriteSize + 1;
		for(int y = yMin; y < yMax; y++){
			for(int x = xMin; x < xMax; x++){
				getTile(x, y).render(x * sheet.spriteSize, y * sheet.spriteSize);
			}
		}
	}
	private void loadTileMap(){
		if(tiles == null){
			BufferedImage im = AssetLoader.getLoader().getImage("/data/Levels/" + name + tileMap);
			int[] tileColors = im.getRGB(0, 0, im.getWidth(), im.getHeight(), null, 0, im.getWidth());
			tiles = new byte[im.getWidth() * im.getHeight()];
			for(int y = 0; y < im.getHeight(); y++){
				for(int x = 0; x < im.getWidth(); x++){
					boolean tileFound = false;
					for(Tile t : Tile.tiles){
						if(tileFound) continue;
						if(t != null && t.getColor() == tileColors[x + y * im.getWidth()]){
							tiles[x + y * im.getWidth()] = t.getID();
							tileFound = true;
						}
					}
					if(!tileFound){
						throw new RuntimeException("COULD NOT LOAD LEVEL " + tileMap);
					}
				}
			}
		} else return;
	}
	public Tile getTile(int x, int y){
		if(x < 0 || x >= width || y < 0 || y >= height) return Tile.tiles[0];
		return Tile.tiles[tiles[x + y * width]];
	}
	public void addConnection(LevelConnection lc){ connections.add(lc); }
	public boolean isEventTile(int x, int y){
		return false;
	}
	public boolean isConnection(int x, int y){
		for(LevelConnection lc: connections){
			if(lc.isPositionOnConnection(x, y)){
				return true;
			}
		}
		return false;
	}
	public void handleWarp(int x, int y){
		LevelConnection levelCon = null;
		for(LevelConnection lc : connections){
			if(lc.isPositionOnConnection(x, y)){
				levelCon = lc;
				break;
			}
		}
		LevelTransitionEvent lte = new LevelTransitionEvent(levelCon.getLevel(), levelCon.getToX(), levelCon.getToY());
		EventCore.getBus().triggerEvent(lte);
	}
	public void setTile(int x, int y, byte tile){
		tiles[x + y * width] = tile;
	}
	public int getWidth(){ return width; }
	public int getHeight(){ return height; }
	public int getTileSize(){ return sheet.spriteSize; }
	
	public static void loadLevel(String levelName){
		Gson gson = AssetLoader.getLoader().getGsonInstance();
		String file = AssetLoader.getLoader().getFileContents("/data/Levels/" + levelName + "/Level.json");
		Level temp = gson.fromJson(file, Level.class);
		temp.loadTileMap();
		SpriteSwitchEvent sse = new SpriteSwitchEvent("TILES", temp.sheet);
		LevelLoadedEvent lle = new LevelLoadedEvent(temp);
		if(!firstLoad){
			EventCore.getBus().triggerEvent(lle);
			firstLoad = true;
		} else {
			EventCore.getBus().queueEvent(sse);
			EventCore.getBus().queueEvent(lle);
		}
	}
	
	public boolean handle(Event event) {
		if(event instanceof XOffsetEvent){
			XOffsetEvent roe = (XOffsetEvent) event;
			xOffset = roe.getOffset();
			return true;
		}
		if(event instanceof YOffsetEvent){
			YOffsetEvent yoe = (YOffsetEvent) event;
			yOffset = yoe.getOffset();
			return true;
		}
		return false;
	}
}
