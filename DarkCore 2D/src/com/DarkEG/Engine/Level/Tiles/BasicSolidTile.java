package com.DarkEG.Engine.Level.Tiles;

public class BasicSolidTile extends BasicTile {
	public BasicSolidTile(int id, int x, int y, int color){
		super(id, x, y, color);
		this.solid = true;
	}
}
