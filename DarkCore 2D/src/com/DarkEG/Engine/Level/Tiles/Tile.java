package com.DarkEG.Engine.Level.Tiles;

import com.google.gson.annotations.Expose;

public abstract class Tile {
	
	public static Tile[] tiles = new Tile[256];
	
	@Expose protected byte id;
	@Expose protected boolean solid;
	protected boolean emitter;
	@Expose protected int color;
	
	public Tile(int id, boolean solid, boolean emitter, int color){
		this.id = (byte)id;
		if(tiles[id] != null) throw new RuntimeException("Duplicate Tile id on " + id);
		this.solid = solid;
		this.emitter = emitter;
		this.color = color;
		tiles[id] = this;
	}
	public byte getID(){ return id; }
	public boolean isSolid(){ return solid; }
	public boolean isEmitter(){ return emitter; }
	public int getColor(){ return color; }
	public abstract void tick();
	public abstract void render(int x, int y);
}
