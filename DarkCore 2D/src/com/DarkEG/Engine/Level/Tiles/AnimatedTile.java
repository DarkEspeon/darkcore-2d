package com.DarkEG.Engine.Level.Tiles;

import com.google.gson.annotations.Expose;

public class AnimatedTile extends BasicTile {
	@Expose private int[][] animationCoords;
	private int currentFrame;
	private long lastIterTime;
	@Expose private int animationDelay;
	
	public AnimatedTile(int id, int[][] animationCoords, int tileColor, int animationDelay){
		super(id, animationCoords[0][0], animationCoords[0][1], tileColor);
		this.animationCoords = animationCoords;
		this.currentFrame = 0;
		this.lastIterTime = System.currentTimeMillis();
		this.animationDelay = animationDelay;
	}
	public void tick(){
		if(System.currentTimeMillis() - this.lastIterTime >= this.animationDelay){
			this.lastIterTime = System.currentTimeMillis();
			currentFrame = (currentFrame + 1) % animationCoords.length;
			this.tileX = animationCoords[currentFrame][0];
			this.tileY = animationCoords[currentFrame][1];
		}
	}
}
