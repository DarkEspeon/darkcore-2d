package com.DarkEG.Engine.Level.Tiles;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.RenderEvent;
import com.google.gson.annotations.Expose;

public class BasicTile extends Tile {
	@Expose protected int tileX, tileY;
	public BasicTile(int id, int x, int y, int color){
		super(id, false, false, color);
		this.tileX = x;
		this.tileY = y;
	}
	public void tick(){}
	public void render(int x, int y){
		RenderEvent e = new RenderEvent("TILES", x, y, tileX, tileY, 0, 1);
		EventCore.getBus().queueEvent(e);
	}
}
