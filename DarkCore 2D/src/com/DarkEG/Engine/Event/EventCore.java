package com.DarkEG.Engine.Event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.DarkEG.Engine.Event.Default.DeadEvent;

public class EventCore {
	private static EventCore ec = null;
	
	private Map<Integer, List<EventListener>> listeners = new HashMap<>();
	private List<Queue<Event>> eventQueues;
	private int numQueues, activeQueue;
	
	public static void createBus(int numQ){
		if(ec != null) return;
		ec = new EventCore(numQ);
	}
	public static EventCore getBus(){ return ec; }
	
	public EventCore(int numQ){
		if(numQ < 2) numQ = 2;
		this.numQueues = numQ;
		this.activeQueue = 0;
		eventQueues = new ArrayList<>();
		for(int i = 0; i < numQueues; i++){
			eventQueues.add(new ConcurrentLinkedQueue<Event>());
		}
	}
	public void addListener(EventListener el, int type){
		if(el == null) return;
		if(!listeners.containsKey(type))
			listeners.put(type, new ArrayList<>());
		else if(listeners.get(type).contains(el))
			throw new RuntimeException("Tried to double register an Event Listener");
		listeners.get(type).add(el);
	}
	public void removeListener(EventListener el, int type){
		if(el == null) return;
		if(!listeners.containsKey(type)) return;
		if(!listeners.get(type).contains(el)) return;
		else listeners.get(type).remove(el);
	}
	public void triggerEvent(Event e){
		if(e == null) return;
		if(!listeners.containsKey(e.getType())) return;
		List<EventListener> listener = listeners.get(e.getType());
		for(EventListener l : listener){
			l.handle(e);
		}
	}
	public void queueEvent(Event e){
		if(e == null) return;
		if(!listeners.containsKey(e.getType())) return;
		eventQueues.get(activeQueue).offer(e);
	}
	public void abortEvent(int eventType, boolean allOfType){
		Queue<Event> queue = eventQueues.get(activeQueue);
		Iterator<Event> i = queue.iterator();
		while(i.hasNext()){
			Event e = i.next();
			if(e.getType() == eventType){
				i.remove();
				if(!allOfType) return;
			}
		}
	}
	public void update(){
		int queueToProcess = activeQueue;
		activeQueue = (activeQueue + 1) % numQueues;
		eventQueues.get(activeQueue).clear();
		
		while(!eventQueues.get(queueToProcess).isEmpty()){
			Event e = eventQueues.get(queueToProcess).poll();
			for(EventListener l : listeners.get(e.getType())){
				if(l.handle(e)){
					e.handleEvent();
				}
			}
			if(!e.wasHandled()){
				this.queueEvent(new DeadEvent(e));
			}
		}
	}
}
