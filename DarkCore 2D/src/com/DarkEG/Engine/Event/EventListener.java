package com.DarkEG.Engine.Event;

public interface EventListener{
	boolean handle(Event event);
}
