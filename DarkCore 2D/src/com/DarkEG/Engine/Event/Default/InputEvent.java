package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;

public abstract class InputEvent extends Event {
	public InputEvent(int eventType) {
		super(eventType);
	}
}
