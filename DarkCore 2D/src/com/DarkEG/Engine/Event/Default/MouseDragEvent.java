package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class MouseDragEvent extends Event{
	private int x, y, button;
	public MouseDragEvent(int x, int y, int button) {
		super(EventID.MOUSEDRAG);
		this.x = x;
		this.y = y;
		this.button = button;
	}
	public int getX(){ return x; }
	public int getY(){ return y; }
	public int getButton(){ return button; }
}
