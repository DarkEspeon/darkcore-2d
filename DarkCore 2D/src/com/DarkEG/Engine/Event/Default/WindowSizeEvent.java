package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class WindowSizeEvent extends Event {
	private int width, height;
	public WindowSizeEvent(int width, int height) {
		super(EventID.WINDOWSIZE);
		this.width = width;
		this.height = height;
	}
	public int getWidth(){ return width; }
	public int getHeight(){ return height; }
}
