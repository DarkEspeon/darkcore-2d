package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class WindowScaleEvent extends Event {
	private int scale;
	public WindowScaleEvent(int scale) {
		super(EventID.WINDOWSCALE);
		this.scale = scale;
	}
	public int getScale(){ return scale; }
}
