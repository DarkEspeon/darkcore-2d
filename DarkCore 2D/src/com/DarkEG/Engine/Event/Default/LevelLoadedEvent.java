package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Level.Level;
import com.DarkEG.Engine.Util.EventID;

public class LevelLoadedEvent extends Event{
	private Level level;
	public LevelLoadedEvent(Level level) {
		super(EventID.LEVELLOADED);
		this.level = level;
	}
	public Level getLevel(){ return level; }
}
