package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class WindowTitleEvent extends Event {
	private String title;
	public WindowTitleEvent(String title) {
		super(EventID.WINDOWTITLE);
		this.title = title;
	}
	public String getTitle(){ return title; }
}
