package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Render.SpriteSheet;
import com.DarkEG.Engine.Util.EventID;

public class SpriteSwitchEvent extends Event {
	private SpriteSheet newSheet;
	private String renderTarget;
	public SpriteSwitchEvent(String target, SpriteSheet newSheet) {
		super(EventID.SPRITESWITCH);
		this.newSheet = newSheet;
		this.renderTarget = target;
	}
	public SpriteSheet getSheet(){ return newSheet; }
	public String getTarget(){ return renderTarget; }
}
