package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Util.EventID;

public class KeyboardEvent extends InputEvent {
	private int keyCode;
	private boolean pressed;
	private boolean shift, ctrl, alt;
	public KeyboardEvent(int keyCode, boolean pressed, boolean shift, boolean ctrl, boolean alt) {
		super(EventID.KEYBOARD);
		this.keyCode = keyCode;
		this.pressed = pressed;
		this.shift = shift;
		this.ctrl = ctrl;
		this.alt = alt;
	}
	public int getKeyCode(){ return keyCode; }
	public boolean wasPressed(){ return pressed; }
	public boolean shiftPressed(){ return shift; }
	public boolean ctrlPressed(){ return ctrl; }
	public boolean altPressed(){ return alt; }
}
