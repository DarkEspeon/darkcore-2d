package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class MouseReleaseEvent extends Event {
	private int x, y, button;
	public MouseReleaseEvent(int x, int y, int button) {
		super(EventID.MOUSERELEASE);
		this.x = x;
		this.y = y;
		this.button = button;
	}
	public int getX(){ return x; }
	public int getY(){ return y; }
	public int getButton(){ return button; }
}
