package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class LevelTransitionEvent extends Event {
	private String newLevel;
	private int toX;
	private int toY;
	public LevelTransitionEvent(String newLevel, int toX, int toY){
		super(EventID.LEVELTRANSITION);
		this.newLevel = newLevel;
		this.toX = toX;
		this.toY = toY;
	}
	public String getLevel(){ return newLevel; }
	public int getToX(){ return toX; }
	public int getToY(){ return toY; }
}
