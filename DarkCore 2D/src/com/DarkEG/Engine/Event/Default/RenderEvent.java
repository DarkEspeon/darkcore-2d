package com.DarkEG.Engine.Event.Default;

import java.util.Map;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class RenderEvent extends Event{
	private int xPos, yPos, tileX, tileY, mirrorDir, scale;
	private Map<Integer, Integer> colorSwap;
	private boolean hasColorSwap;
	private String layerName;
	private int textColor = 0;
	private String textContent = "";
	
	public RenderEvent(String layerTarget, int xPos, int yPos, int tileX, int tileY, int mirrorDir, int scale) {
		super(EventID.RENDER);
		this.layerName = layerTarget;
		this.xPos = xPos;
		this.yPos = yPos;
		this.tileX = tileX;
		this.tileY = tileY;
		this.mirrorDir = mirrorDir;
		this.scale = scale;
		this.colorSwap = null;
		this.hasColorSwap = false;
	}
	public RenderEvent(String layerTarget, int xPos, int yPos, int tileX, int tileY, int mirrorDir, int scale, Map<Integer, Integer> colorSwap) {
		super(EventID.RENDER);
		this.layerName = layerTarget;
		this.xPos = xPos;
		this.yPos = yPos;
		this.tileX = tileX;
		this.tileY = tileY;
		this.mirrorDir = mirrorDir;
		this.scale = scale;
		if(colorSwap == null){
			this.colorSwap = null;
			this.hasColorSwap = false;
		} else {
			this.colorSwap = colorSwap;
			this.hasColorSwap = true;
		}
	}
	public RenderEvent(String layerTarget, int xPos, int yPos, String message, int color){
		super(EventID.RENDER);
		this.layerName = layerTarget;
		this.xPos = xPos;
		this.yPos = yPos;
		this.textColor = color;
		this.textContent = message;
	}
	public String getLayerTarget(){ return layerName; }
	public int getXPos(){ return xPos; }
	public int getYPos(){ return yPos; }
	public int getTileX(){ return tileX; }
	public int getTileY(){ return tileY; }
	public int getMirrorDir(){ return mirrorDir; }
	public int getScale(){ return scale; }
	public Map<Integer, Integer> getColorSwap(){ return colorSwap; }
	public boolean hasColorSwap(){ return hasColorSwap; }
	public boolean isText(){ return !textContent.equals(""); }
	public String getText(){ return textContent; }
	public int getTextColor(){ return textColor; }
}
