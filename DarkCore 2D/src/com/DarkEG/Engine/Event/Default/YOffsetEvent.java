package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class YOffsetEvent extends Event{
	private int yOffset;
	
	public YOffsetEvent(int yOffset) {
		super(EventID.YRENDEROFFSET);
		this.yOffset = yOffset;
	}
	public int getOffset(){ return yOffset; }
}
