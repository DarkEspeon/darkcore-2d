package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class DeadEvent extends Event {
	private Event e;
	public DeadEvent(Event deadEvent) {
		super(EventID.DEAD);
		this.e = deadEvent;
	}
	public Event getDeadEvent(){ return e; }

}
