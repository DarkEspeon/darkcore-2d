package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class MousePressEvent extends Event {
	private int x, y, button;
	public MousePressEvent(int x, int y, int button) {
		super(EventID.MOUSEPRESS);
		this.x = x;
		this.y = y;
		this.button = button;
	}
	public int getX(){ return x; }
	public int getY(){ return y; }
	public int getButton(){ return button; }
}
