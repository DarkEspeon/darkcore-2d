package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class LoadLevelEvent extends Event {
	private String levelName;
	public LoadLevelEvent(String level){
		super(EventID.LOADLEVEL);
		this.levelName = level;
	}
	public String getLevelName(){ return levelName; }
}
