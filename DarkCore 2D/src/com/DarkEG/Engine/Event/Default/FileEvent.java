package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;

public class FileEvent extends Event {
	private String filePath;
	private String name;
	public FileEvent(String name, String filePath) {
		super(0xf004);
		this.name = name;
		this.filePath = filePath;
	}
	public String getName(){ return name; }
	public String getFilePath(){ return filePath; }
}
