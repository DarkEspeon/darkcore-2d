package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class KeybindEvent extends Event{
	private String name;
	private boolean pressed;
	public KeybindEvent(String name, boolean pressed){
		super(EventID.KEYBIND);
		this.name = name;
		this.pressed = pressed;
	}
	public String getName(){ return name; }
	public boolean isPressed(){ return pressed; }
}
