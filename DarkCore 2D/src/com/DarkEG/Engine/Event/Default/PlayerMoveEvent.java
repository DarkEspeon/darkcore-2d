package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class PlayerMoveEvent extends Event{
	private int x, y;
	public PlayerMoveEvent(int x, int y) {
		super(EventID.PLAYERMOVE);
		this.x = x;
		this.y = y;
	}
	public int getX(){ return x; }
	public int getY(){ return y; }
}
