package com.DarkEG.Engine.Event.Default;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Util.EventID;

public class XOffsetEvent extends Event{
	private int xOffset;
	
	public XOffsetEvent(int xOffset) {
		super(EventID.XRENDEROFFSET);
		this.xOffset = xOffset;
	}
	public int getOffset(){ return xOffset; }
}
