package com.DarkEG.Engine.Event;

public class Event {
	private int eventType; //UNIQUE ID FOR EACH EVENT TYPE!!!!
	private boolean handled;
	public Event(int eventType){
		this.eventType = eventType;
		handled = false;
	}
	public int getType(){ return eventType; }
	public boolean wasHandled(){ return handled; }
	public void handleEvent(){ handled = true; }
}
