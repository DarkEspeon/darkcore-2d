package com.DarkEG.Engine.Exception;

public class OverrideException extends Exception {
	private static final long serialVersionUID = 6110349302819344278L;
	public OverrideException() {
        super();
    }
    public OverrideException(String message) {
        super(message);
    }
    public OverrideException(String message, Throwable cause) {
        super(message, cause);
    }
    public OverrideException(Throwable cause) {
        super(cause);
    }
}
