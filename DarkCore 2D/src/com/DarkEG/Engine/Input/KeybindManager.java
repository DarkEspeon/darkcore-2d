package com.DarkEG.Engine.Input;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.KeyboardEvent;
import com.google.gson.annotations.Expose;

public class KeybindManager implements EventListener {
	public static Map<String, Integer> keyToInt;
	static {
		keyToInt = new HashMap<>();
		keyToInt.put("A", KeyEvent.VK_A);
		keyToInt.put("B", KeyEvent.VK_B);
		keyToInt.put("C", KeyEvent.VK_C);
		keyToInt.put("D", KeyEvent.VK_D);
		keyToInt.put("E", KeyEvent.VK_E);
		keyToInt.put("F", KeyEvent.VK_F);
		keyToInt.put("G", KeyEvent.VK_G);
		keyToInt.put("H", KeyEvent.VK_H);
		keyToInt.put("I", KeyEvent.VK_I);
		keyToInt.put("J", KeyEvent.VK_J);
		keyToInt.put("K", KeyEvent.VK_K);
		keyToInt.put("L", KeyEvent.VK_L);
		keyToInt.put("M", KeyEvent.VK_M);
		keyToInt.put("N", KeyEvent.VK_N);
		keyToInt.put("O", KeyEvent.VK_O);
		keyToInt.put("P", KeyEvent.VK_P);
		keyToInt.put("Q", KeyEvent.VK_Q);
		keyToInt.put("R", KeyEvent.VK_R);
		keyToInt.put("S", KeyEvent.VK_S);
		keyToInt.put("T", KeyEvent.VK_T);
		keyToInt.put("U", KeyEvent.VK_U);
		keyToInt.put("V", KeyEvent.VK_V);
		keyToInt.put("W", KeyEvent.VK_W);
		keyToInt.put("X", KeyEvent.VK_X);
		keyToInt.put("Y", KeyEvent.VK_Y);
		keyToInt.put("Z", KeyEvent.VK_Z);
		
		keyToInt.put("0", KeyEvent.VK_0);
		keyToInt.put("1", KeyEvent.VK_1);
		keyToInt.put("2", KeyEvent.VK_2);
		keyToInt.put("3", KeyEvent.VK_3);
		keyToInt.put("4", KeyEvent.VK_4);
		keyToInt.put("5", KeyEvent.VK_5);
		keyToInt.put("6", KeyEvent.VK_6);
		keyToInt.put("7", KeyEvent.VK_7);
		keyToInt.put("8", KeyEvent.VK_8);
		keyToInt.put("9", KeyEvent.VK_9);

		keyToInt.put("ALT", KeyEvent.VK_ALT);
		keyToInt.put("CAPS", KeyEvent.VK_CAPS_LOCK);
		keyToInt.put("CTRL", KeyEvent.VK_CONTROL);
		keyToInt.put("SHIFT", KeyEvent.VK_SHIFT);
		
		keyToInt.put("UP", KeyEvent.VK_UP);
		keyToInt.put("LEFT", KeyEvent.VK_LEFT);
		keyToInt.put("DOWN", KeyEvent.VK_DOWN);
		keyToInt.put("RIGHT", KeyEvent.VK_RIGHT);
	}
	@Expose private List<Keybind> keybinds;
	private static KeybindManager manager;
	public static void createManager(){ manager = new KeybindManager(); }
	public static KeybindManager getKeybinds(){ return manager; }
	public KeybindManager(){
		keybinds = new ArrayList<>();
		EventCore.getBus().addListener(this, 0xf000);
	}
	public void registerKeybind(Keybind newKeybind){ 
		if(!keybinds.contains(newKeybind)) keybinds.add(newKeybind); 
	}
	public void deregisterKeybind(Keybind k){
		if(keybinds.contains(k)) keybinds.remove(k);
	}
	public boolean handle(Event event) {
		if(event instanceof KeyboardEvent){
			KeyboardEvent kevent = (KeyboardEvent) event;
			for(Keybind k : keybinds) k.checkKeybind(kevent);
			return true;
		}
		return false;
	}
	
}
