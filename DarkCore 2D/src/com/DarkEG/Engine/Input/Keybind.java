package com.DarkEG.Engine.Input;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.KeybindEvent;
import com.DarkEG.Engine.Event.Default.KeyboardEvent;
import com.google.gson.annotations.Expose;

public class Keybind {
	@Expose private String name;
	@Expose private int keyCode = -1;
	@Expose private boolean shift, ctrl, alt;
	@Expose private long cooldown = 0;
	private long lastTime = 0;
	private boolean pressed = false;
	public Keybind(String name, int keyCode, boolean shift, boolean ctrl, boolean alt, long cooldown){
		this.name = name;
		this.keyCode = keyCode;
		this.shift = shift;
		this.ctrl = ctrl;
		this.alt = alt;
		this.cooldown = cooldown;
		this.lastTime = System.currentTimeMillis();
	}
	public void checkKeybind(KeyboardEvent e){
		if(System.currentTimeMillis() - lastTime > cooldown && e.getKeyCode() == this.keyCode && (!pressed == e.wasPressed())){
			if((!ctrl || e.ctrlPressed()) && (!shift || e.shiftPressed()) && (!alt || e.altPressed())){
				if(e.wasPressed()){
					lastTime = System.currentTimeMillis();
					pressed = true;
				} else {
					pressed = false;
				}
				KeybindEvent event = new KeybindEvent(name, pressed);
				EventCore.getBus().triggerEvent(event);
			}
		}
	}
}
