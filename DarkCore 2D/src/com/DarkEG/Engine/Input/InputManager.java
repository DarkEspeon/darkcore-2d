package com.DarkEG.Engine.Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.KeyboardEvent;
import com.DarkEG.Engine.Event.Default.MouseDragEvent;
import com.DarkEG.Engine.Event.Default.MousePressEvent;
import com.DarkEG.Engine.Event.Default.MouseReleaseEvent;

public class InputManager implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {
	//Wheel Events
	public void mouseWheelMoved(MouseWheelEvent e) {}
	//Motion Events
	public void mouseDragged(MouseEvent e) {
		MouseDragEvent event = new MouseDragEvent(e.getX(), e.getY(), e.getButton());
		EventCore.getBus().queueEvent(event);
	}
	public void mouseMoved(MouseEvent e) {}
	//Mouse Events
	public void mouseClicked(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {
		MousePressEvent event = new MousePressEvent(e.getX(), e.getY(), e.getButton());
		EventCore.getBus().queueEvent(event);
	}
	public void mouseReleased(MouseEvent e) {
		MouseReleaseEvent event = new MouseReleaseEvent(e.getX(), e.getY(), e.getButton());
		EventCore.getBus().queueEvent(event);
	}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	//Key Event
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e) {
		KeyboardEvent event = new KeyboardEvent(e.getKeyCode(), true, e.isShiftDown(), e.isControlDown(), e.isAltDown());
		EventCore.getBus().queueEvent(event);
	}
	public void keyReleased(KeyEvent e) {
		KeyboardEvent event = new KeyboardEvent(e.getKeyCode(), false, e.isShiftDown(), e.isControlDown(), e.isAltDown());
		EventCore.getBus().queueEvent(event);
	}
}
