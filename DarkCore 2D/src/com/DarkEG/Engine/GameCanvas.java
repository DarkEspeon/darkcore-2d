package com.DarkEG.Engine;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Input.InputManager;
import com.DarkEG.Engine.Util.EventID;

public class GameCanvas extends Canvas implements EventListener {
	private static final long serialVersionUID = -8150335341203545320L;
	private BufferStrategy bs;
	private Graphics g;
	private int gwidth, gheight, gscale;
	private BufferedImage image;
	private int[] pixels;
	private boolean visible = false;;
	
	public GameCanvas(){
		gwidth = 1;
		gheight = 1;
		gscale = 1;
		EventCore.getBus().addListener(this, EventID.WINDOWSIZE);
		EventCore.getBus().addListener(this, EventID.WINDOWSCALE);
	}
	public void init(boolean visible){
		this.visible = visible;
		updateWindow();
		setFocusable(false);
		super.setVisible(visible);
	}
	private void changeSize(int width, int height){
		this.gwidth = width;
		this.gheight = height;
		updateWindow();
	}
	private void changeScale(int scale){
		this.gscale = scale;
		updateWindow();
	}
	private void updateWindow(){
		setMinimumSize(new Dimension(gwidth * gscale, gheight * gscale));
		setMaximumSize(new Dimension(gwidth * gscale, gheight * gscale));
		setPreferredSize(new Dimension(gwidth * gscale, gheight * gscale));
		
		image = new BufferedImage(gwidth, gheight, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	}
	public void setVisible(boolean visible){
		this.visible = visible;
		super.setVisible(visible);
	}
	public void startFrame(){
		if(!visible) return;
		bs = getBufferStrategy();
		if(bs == null){
			createBufferStrategy(3);
			return;
		}
		g = bs.getDrawGraphics();
	}
	public void render(int[] pixels){
		if(!visible) return;
		if(bs == null) return;
		for(int y = 0; y < gheight; y++){
			for(int x = 0; x < gwidth; x++){
				this.pixels[x + y * gwidth] = pixels[x + y * gwidth];
			}
		}
		
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}
	public void endFrame(){
		if(!visible) return;
		if(bs == null) return;
		g.dispose();
		bs.show();
	}
	public boolean handle(Event e){
		if(e instanceof WindowSizeEvent){
			WindowSizeEvent wse = (WindowSizeEvent) e;
			changeSize(wse.getWidth(), wse.getHeight());
			return true;
		}
		if(e instanceof WindowScaleEvent){
			WindowScaleEvent wse = (WindowScaleEvent) e;
			changeScale(wse.getScale());
			return true;
		}
		return false;
	}
}
