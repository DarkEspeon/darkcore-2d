package com.DarkEG.Engine;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Process.DarkProcess;
import com.DarkEG.Engine.Process.ProcessManager;

public abstract class Game {
	protected static ProcessManager pm;
	protected int width, height;
	private boolean devMode;
	
	public void dataInit(ProcessManager pm, EngineCore ec, boolean devMode){
		Game.pm = pm;
		this.devMode = devMode;
		onDataInit(ec);
	}
	public void graphicsInit(GameWindow window, GameCanvas canvas){
		onGraphicsInit(window, canvas);
	}
	public void tick(float delta){
		onTick(delta);
		pm.updateProcesses(delta);
		EventCore.getBus().update();
	}
	protected abstract void onDataInit(EngineCore ec);
	protected abstract void onGraphicsInit(GameWindow window, GameCanvas canvas);
	public abstract void onTick(float delta);
	public abstract void onRender();
	public abstract int[] getScreenPixels();
	public static void attachProcess(DarkProcess p){
		pm.attachProcess(p);
	}
	public boolean isHeadless(){ return false; }
	public boolean isDevMode(){ return devMode; }
}
