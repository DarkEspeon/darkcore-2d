package com.DarkEG.Engine.Asset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class XMLParser {
	public abstract void parse(String file, Element rootElement);
	protected final List<Map<String, String>> getList(Element element, String tag, String... attribs){
		List<Map<String, String>> result = new ArrayList<>();
		List<Element> children = getElements(element, tag);
		for(Element e : children){
			Map<String, String> temp = new HashMap<>();
			for(String s : attribs){
				temp.put(s,  e.hasAttribute(s) ? e.getAttribute(s) : "");
			}
			result.add(temp);
		}
		return result;
	}
	protected final List<Element> getElements(Element root, String children){
		List<Element> result = new ArrayList<>();
		NodeList nl = root.getChildNodes();
		if(nl != null && nl.getLength() > 0){
			for(int i = 0; i < nl.getLength(); i++){
				Node n = (Node) nl.item(i);
				if(n.getNodeType() == Node.ELEMENT_NODE && children.equals(n.getNodeName())) result.add((Element) n);
			}
		}
		return result;
	}
	protected final boolean elementExists(Element root, String name){
		return getElements(root, name).size() != 0;
	}
	protected final long parseLong(String in){
		long out = 0;
		if(in.startsWith("0x")){
			out = Long.parseLong(in.substring(2), 16);
		} else out = Long.parseLong(in);
		
		return out;
	}
	protected final int parseInt(String in){
		int out = 0;
		if(in.startsWith("0x")){
			out = Integer.parseInt(in.substring(2), 16);
		} else out = Integer.parseInt(in);
		return out;
	}
}
