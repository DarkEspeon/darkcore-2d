package com.DarkEG.Engine.Asset;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;

public class FilesystemAssetPath implements AssetPath {
	private final File root;
	public FilesystemAssetPath(File root){
		this.root = root;
	}
	public InputStream getResourceAsStream(String ref) {
		try {
			File file = new File(root, ref);
			if(!file.exists())
				file = new File(ref);
			if(!file.exists())
				return null;
			return new FileInputStream(file);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public URL getResource(String ref) {
		try {
			File file = new File(root, ref);
			if(!file.exists()) file = new File(ref);
			if(!file.exists()) return null;
			return file.toURI().toURL();
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
