package com.DarkEG.Engine.Asset;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLLoader {
	private static XMLLoader loader;
	public static void initXMLLoader(){ loader = new XMLLoader(); }
	public static XMLLoader getXMLLoader(){ return loader; }
	
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private Map<String, Map<String, XMLParser>> typedParser = new HashMap<>();
	private Map<String, XMLParser> parsers = new HashMap<>();
	
	public void parseXML(String ref){
		parseXML(AssetLoader.getLoader().getResourceAsFile(ref));
	}
	public void parseXML(File f){
		DocumentBuilder db = null;
		Document dom = null;
		try {
			db = dbf.newDocumentBuilder();
			dom = db.parse(f);
		} catch (Exception e){
			e.printStackTrace();
		}
		if(dom == null) throw new RuntimeException("XML file " + f.getName() + " didn't open properly!");
		else {
			Element rootElement = dom.getDocumentElement();
			String tagName = rootElement.getTagName();
			if(parsers.containsKey(tagName)){
				parsers.get(tagName).parse(f.getName(), rootElement);
			} else if(typedParser.containsKey(tagName) && rootElement.hasAttribute("Type")){
				String type = rootElement.getAttribute("Type");
				if(typedParser.get(tagName).containsKey(type)) typedParser.get(tagName).get(type).parse(f.getName(), rootElement);
			}
		}
	}
	public void registerXMLParser(String rootNode, XMLParser parser){
		if(parsers.containsKey(rootNode)) return;
		parsers.put(rootNode, parser);
	}
	public void unregisterXMLParser(String rootNode){ parsers.remove(rootNode); }
	public void registerTypedXMLParser(String rootNode, String type, XMLParser parser){
		if(!typedParser.containsKey(rootNode)) typedParser.put(rootNode, new HashMap<>());
		typedParser.get(rootNode).put(type, parser);
	}
	public void unregisterTypedXMLParser(String rootNode, String type){
		typedParser.get(rootNode).remove(type);
		if(typedParser.get(rootNode).isEmpty()) typedParser.remove(rootNode);
	}
}
