package com.DarkEG.Engine.Asset;

import java.io.InputStream;
import java.net.URL;

public class ClasspathAssetPath implements AssetPath {
	public InputStream getResourceAsStream(String ref){
		String cpRef = ref.replace('\\', '/');
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(cpRef);
	}
	public URL getResource(String ref){
		String cpRef = ref.replace('\\', '/');
		return Thread.currentThread().getContextClassLoader().getResource(cpRef);
	}
}
