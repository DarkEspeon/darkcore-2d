package com.DarkEG.Engine.Asset;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.DarkEG.Engine.Level.Tiles.Tile;
import com.DarkEG.Engine.Render.SpriteSheet;
import com.DarkEG.Engine.Util.Setting;
import com.DarkEG.Engine.Util.WindowSettings;
import com.DarkEG.Engine.Util.Json.SettingDeserializer;
import com.DarkEG.Engine.Util.Json.SpriteSheetDeserializer;
import com.DarkEG.Engine.Util.Json.TileDeserializer;
import com.DarkEG.Engine.Util.Json.WindowSettingDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AssetLoader {
	private static AssetLoader loader;
	public static void initLoader() { loader = new AssetLoader(); }
	public static AssetLoader getLoader() { return loader; }
	
	private List<AssetPath> paths = new ArrayList<>();
	private Gson gson;
	private AssetLoader() { 
		GsonBuilder gsonBuild = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation();
		gsonBuild.registerTypeAdapter(SpriteSheet.class, new SpriteSheetDeserializer());
		gsonBuild.registerTypeAdapter(WindowSettings.class, new WindowSettingDeserializer());
		gsonBuild.registerTypeAdapter(Setting.class, new SettingDeserializer());
		gsonBuild.registerTypeAdapter(Tile[].class, new TileDeserializer());
		gson = gsonBuild.create();
	}
	public void addAssetPath(AssetPath ap){ paths.add(ap); }
	public void removeAssetPath(AssetPath ap){ paths.remove(ap); }
	public void removeAllAssetPaths(){ paths.clear(); }
	public File getResourceAsFile(String ref){
		try {
			URI uri = getResource(ref).toURI();
			return new File(uri);
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public URI getResourceAsURI(String ref){
		try {
			URI uri = getResource(ref).toURI();
			return uri;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public InputStream getResourceAsStream(String ref){
		InputStream in = null;
		for(AssetPath location : paths){
			in = location.getResourceAsStream(ref);
			if(in != null) break;
		}
		if(in == null) return null;
		return new BufferedInputStream(in);
	}
	public URL getResource(String ref){
		URL url = null;
		for(AssetPath location : paths){
			url = location.getResource(ref);
			if(url != null) break;
		}
		return url;
	}
	public String getFileContents(String ref){
		try {
			String contents = new String(Files.readAllBytes(Paths.get(getResourceAsURI(ref))));
			return contents;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public boolean writeToFile(String ref, String data){
		List<String> dataList = new ArrayList<>();
		dataList.add(data);
		try {
			Files.write(Paths.get(ref), dataList);
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}
	public Gson getGsonInstance(){ return gson; }
	public BufferedImage getImage(String ref){
		BufferedImage im = null;
		try {
			im = ImageIO.read(getResourceAsStream(ref));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return im;
	}
}
