package com.DarkEG.Engine.Asset;

import java.io.InputStream;
import java.net.URL;

public interface AssetPath {
	InputStream getResourceAsStream(String ref);
	URL getResource(String ref);
}
