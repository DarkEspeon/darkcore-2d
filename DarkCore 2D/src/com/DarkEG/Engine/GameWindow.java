package com.DarkEG.Engine;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Event.Default.WindowTitleEvent;
import com.DarkEG.Engine.Input.InputManager;
import com.DarkEG.Engine.Util.EventID;

public class GameWindow implements EventListener {
	private JFrame frame;
	private int width;
	private int height;
	private int scale;
	private String title;
	public GameWindow(){
		width = 1;
		height = 1;
		scale = 1;
		EventCore.getBus().addListener(this, EventID.WINDOWSIZE);
		EventCore.getBus().addListener(this, EventID.WINDOWSCALE);
		EventCore.getBus().addListener(this, EventID.WINDOWTITLE);
	}
	public void init(boolean visible){
		frame = new JFrame();
		changeSize();
		setTitle();
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(visible);
		if(!visible){
			frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		} else {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.requestFocus();
		}
		InputManager im = new InputManager();
		frame.addKeyListener(im);
	}
	public void setVisible(boolean visible){
		frame.setVisible(visible);
	}
	public void add(Component c){
		frame.add(c);
	}
	private void changeSize(){
		if(frame == null) return;
		frame.setMaximumSize(new Dimension(width * scale, height * scale));
		frame.setMinimumSize(new Dimension(width * scale, height * scale));
		frame.setPreferredSize(new Dimension(width * scale, height * scale));
	}
	private void setTitle(){
		if(frame == null) return;
		frame.setTitle(title);
	}
	public void addWindowListener(WindowListener wl){
		frame.addWindowListener(wl);
	}
	public boolean handle(Event e){
		if(e instanceof WindowSizeEvent){
			WindowSizeEvent wse = (WindowSizeEvent) e;
			width = wse.getWidth();
			height = wse.getHeight();
			changeSize();
			return true;
		}
		if(e instanceof WindowScaleEvent){
			WindowScaleEvent wse = (WindowScaleEvent) e;
			scale = wse.getScale();
			changeSize();
			return true;
		}
		if(e instanceof WindowTitleEvent){
			WindowTitleEvent wte = (WindowTitleEvent) e;
			this.title = wte.getTitle();
			setTitle();
			return true;
		}
		return false;
	}
}
