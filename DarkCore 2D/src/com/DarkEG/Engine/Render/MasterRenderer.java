package com.DarkEG.Engine.Render;

import java.util.ArrayList;
import java.util.List;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Util.EventID;

public class MasterRenderer implements EventListener{
	private List<RenderLayer> layers = new ArrayList<>();
	private int width, height;
	private int[] pixels;
	private SpriteSheet spriteSheet;
	private boolean renderingLayers = false;
	
	public MasterRenderer(SpriteSheet sheet){
		this.spriteSheet = sheet;
		width = 1;
		height = 1;
		EventCore.getBus().addListener(this, EventID.WINDOWSIZE);
	}
	public void init(){
		pixels = new int[width * height];
	}
	public void createNewLayer(String layerName){
		createNewLayer(layerName, true, true);
	}
	public void createNewLayer(String layerName, boolean offset){
		createNewLayer(layerName, offset, offset);
	}
	public void createNewLayer(String layerName, SpriteSheet sheet){
		createNewLayer(layerName, sheet, true, true);
	}
	public void createNewLayer(String layerName, SpriteSheet sheet, boolean offset){
		createNewLayer(layerName, sheet, offset, offset);
	}
	public void createNewLayer(String layerName, boolean xOffset, boolean yOffset){
		RenderLayer newLayer = new RenderLayer(layerName, spriteSheet, width, height, xOffset, yOffset);
		layers.add(newLayer);
	}
	public void createNewLayer(String layerName, SpriteSheet sheet, boolean xOffset, boolean yOffset){
		RenderLayer newLayer = new RenderLayer(layerName, sheet, width, height, xOffset, yOffset);
		layers.add(newLayer);
	}
	public void createNewTextLayer(String layerName, boolean xOffset, boolean yOffset){
		RenderLayer newLayer = new RenderLayer(layerName, width, height, xOffset, yOffset);
		layers.add(newLayer);
	}
	public void render(){
		renderingLayers = true;
		for(RenderLayer rl : layers){
			int[] layerPixel = rl.getPixels();
			for(int y = 0; y < height; y++){
				for(int x = 0; x < width; x++){
					if(layerPixel[x + y * width] != 0xFFFF00FF){
						pixels[x + y * width] = layerPixel[x + y * width];
					}
				}
			}
			rl.clear();
		}
		renderingLayers = false;
	}
	public int[] getPixels(){
		return pixels;
	}
	private void changeSize(int width, int height){
		while(renderingLayers){}
		this.width = width;
		this.height = height;
		pixels = new int[width * height];
	}
	public boolean handle(Event e){
		if(e instanceof WindowSizeEvent){
			WindowSizeEvent wse = (WindowSizeEvent) e;
			changeSize(wse.getWidth(), wse.getHeight());
			return true;
		}
		return false;
	}
}
