package com.DarkEG.Engine.Render;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Map;

import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.RenderEvent;
import com.DarkEG.Engine.Event.Default.SpriteSwitchEvent;
import com.DarkEG.Engine.Event.Default.XOffsetEvent;
import com.DarkEG.Engine.Event.Default.YOffsetEvent;
import com.DarkEG.Engine.Util.EventID;

public class RenderLayer implements EventListener {
	private static final int BIT_MIRROR_X = 0x01;
	private static final int BIT_MIRROR_Y = 0x02;
	
	private String layerName;
	private int width, height;
	private int[] pixels;
	private int xOffset = 0, yOffset = 0;
	
	private BufferedImage textImage;
	private int[] imagePixels;
	
	private SpriteSheet sheet;
	public RenderLayer(String layerName, SpriteSheet sheet, int width, int height, boolean xOffset, boolean yOffset){ //NON TEXT LAYER
		this.layerName = layerName;
		this.sheet = sheet;
		this.width = width;
		this.height = height;
		this.pixels = new int[width * height];
		EventCore.getBus().addListener(this, EventID.RENDER);
		EventCore.getBus().addListener(this, EventID.SPRITESWITCH);
		textImage = null;
		imagePixels = null;
		if(xOffset){
			EventCore.getBus().addListener(this, EventID.XRENDEROFFSET);
		}
		if(yOffset){
			EventCore.getBus().addListener(this, EventID.YRENDEROFFSET);
		}
	}
	public RenderLayer(String layerName, int width, int height, boolean xOffset, boolean yOffset){ //TEXT LAYER
		this.layerName = layerName;
		this.width = width;
		this.height = height;
		this.pixels = new int[width * height];
		EventCore.getBus().addListener(this, EventID.RENDER);
		EventCore.getBus().addListener(this, EventID.SPRITESWITCH);
		textImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		imagePixels = ((DataBufferInt)textImage.getRaster().getDataBuffer()).getData();
		if(xOffset){
			EventCore.getBus().addListener(this, EventID.XRENDEROFFSET);
		}
		if(yOffset){
			EventCore.getBus().addListener(this, EventID.YRENDEROFFSET);
		}
	}
	private void render(int xPos, int yPos, int tileX, int tileY, int mirrorDir, int scale){
		xPos -= xOffset;
		yPos -= yOffset;
		boolean mirrorX = (mirrorDir & BIT_MIRROR_X) > 0;
		boolean mirrorY = (mirrorDir & BIT_MIRROR_Y) > 0;
		int scaleMap = scale - 1;
		int tileOffset = (tileX * sheet.spriteSize) + (tileY * sheet.spriteSize) * sheet.width;
		for(int y = 0; y < sheet.spriteSize; y++){
			int ySheet = y;
			if(mirrorY)
				ySheet = sheet.spriteSize - 1 - y;
			int yPixel = y + yPos + (y * scaleMap) - ((scaleMap * sheet.spriteSize) / 2);
			for(int x = 0; x < sheet.spriteSize; x++){
				int xSheet = x;
				if(mirrorX)
					xSheet = sheet.spriteSize - 1 - x;
				int xPixel = x + xPos + (x * scaleMap) - ((scaleMap * sheet.spriteSize) / 2);
				int col = sheet.pixels[xSheet + ySheet * sheet.width + tileOffset];
				for(int yScale = 0; yScale < scale; yScale++){
					if(yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for(int xScale = 0; xScale < scale; xScale++){
						if(xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if(col != 0xFFFF00FF){
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = col;
						}
					}
				}
			}
		}
	}
	private void render(int xPos, int yPos, int tileX, int tileY, int mirrorDir, int scale, Map<Integer, Integer> colorSwap){
		xPos -= xOffset;
		yPos -= yOffset;
		boolean mirrorX = (mirrorDir & BIT_MIRROR_X) > 0;
		boolean mirrorY = (mirrorDir & BIT_MIRROR_Y) > 0;
		int scaleMap = scale - 1;
		int tileOffset = (tileX * sheet.spriteSize) + (tileY * sheet.spriteSize) * sheet.width;
		for(int y = 0; y < sheet.spriteSize; y++){
			int ySheet = y;
			if(mirrorY)
				ySheet = sheet.spriteSize - 1 - y;
			int yPixel = y + yPos + (y * scaleMap) - ((scaleMap * sheet.spriteSize) / 2);
			for(int x = 0; x < sheet.spriteSize; x++){
				int xSheet = x;
				if(mirrorX)
					xSheet = sheet.spriteSize - 1 - x;
				int xPixel = x + xPos + (x * scaleMap) - ((scaleMap * sheet.spriteSize) / 2);
				int col = sheet.pixels[xSheet + ySheet * sheet.width + tileOffset];
				if(colorSwap.containsKey(col)) col = colorSwap.get(col);
				for(int yScale = 0; yScale < scale; yScale++){
					if(yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for(int xScale = 0; xScale < scale; xScale++){
						if(xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if(col != 0xFFFF00FF){
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = col;
						}
					}
				}
			}
		}
	}
	private void render(int xPos, int yPos, String message, int color){
		if(textImage == null){ 
			throw new RuntimeException("This was NOT initialized as a TEXT LAYER!");
		}
		Graphics g = textImage.getGraphics();
		int xWidth = g.getFontMetrics().stringWidth(message) + 2;
		int yHeight = g.getFontMetrics().getHeight() + 2;
		g.setColor(new Color(0xff00ff));
		g.fillRect(0, 0, width, height);
		g.setColor(new Color(color));
		g.drawString(message, 1, yHeight - 1);
		for(int x = 0; x < xWidth; x++){
			for(int y = 0; y < yHeight; y++){
				pixels[(xPos + x) + (yPos + y) * width] = imagePixels[x + y * width];
			}
		}
	}
	private void setOffset(int xOffset, int yOffset){
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	public int[] getPixels(){ return pixels; }
	public void clear(){
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				pixels[x + y * width] = 0xFFFF00FF;
			}
		}
	}
	public boolean handle(Event e){
		if(e instanceof RenderEvent){
			RenderEvent re = (RenderEvent) e;
			if(re.getLayerTarget().equalsIgnoreCase(layerName)){
				if(re.isText()){
					render(re.getXPos(), re.getYPos(), re.getText(), re.getTextColor());
					return true;
				}
				else if(!re.hasColorSwap()){
					render(re.getXPos(), re.getYPos(), re.getTileX(), re.getTileY(), re.getMirrorDir(), re.getScale());
					return true;
				} else {
					render(re.getXPos(), re.getYPos(), re.getTileX(), re.getTileY(), re.getMirrorDir(), re.getScale(), re.getColorSwap());
					return true;
				}
			}
		}
		if(e instanceof XOffsetEvent){
			XOffsetEvent roe = (XOffsetEvent) e;
			setOffset(roe.getOffset(), yOffset);
			return true;
		}
		if(e instanceof YOffsetEvent){
			YOffsetEvent yoe = (YOffsetEvent) e;
			setOffset(xOffset, yoe.getOffset());
		}
		if(e instanceof SpriteSwitchEvent){
			SpriteSwitchEvent sse = (SpriteSwitchEvent) e;
			if(sse.getTarget().equalsIgnoreCase(layerName))
				if(sse.getSheet() != null) this.sheet = sse.getSheet();
			return true;
		}
		return false;
	}
}
