package com.DarkEG.Engine.Render;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.gson.annotations.Expose;

public class SpriteSheet {
	
	private static Map<String, SpriteSheet> sheets = new HashMap<>();
	
	@Expose public String path;
	public int width;
	public int height;
	@Expose public int spriteSize;
	@Expose public String sheetName;
	public int spriteX, spriteY;
	public int[] pixels = null;
	
	private SpriteSheet(String path, String sheetName, int spriteSize){
		BufferedImage image = null;
		try {
			image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
		} catch (IOException e){
			e.printStackTrace();
		}
		if(image == null) {
			System.out.println("FAILURE");
			return;
		}
		this.sheetName = sheetName;
		this.path = path;
		this.width = image.getWidth();
		this.height = image.getHeight();

		this.spriteSize = spriteSize;
		spriteX = width / spriteSize;
		spriteY = height / spriteSize;
		
		pixels = image.getRGB(0, 0, width, height, null, 0, width);
	}
	public static SpriteSheet getSheet(String path, String sheetName, int spriteSize){
		if(sheets.containsKey(sheetName)) return sheets.get(sheetName);
		else {
			SpriteSheet res = new SpriteSheet(path, sheetName, spriteSize);
			sheets.put(sheetName, res);
			return res;
		}
	}
}
