package com.DarkEG.Engine.Util;

import com.google.gson.annotations.Expose;

public class Data<T> {
	@Expose private T data;
	public Data(T data){
		this.data = data;
	}
	public T getData(){
		return data;
	}
}
