package com.DarkEG.Engine.Util;

import com.google.gson.annotations.Expose;

public class WindowSettings {
	@Expose private String title;
	@Expose private int width, height;
	@Expose private int scale;
	public WindowSettings(String title, int width, int height, int scale){
		this.title = title;
		this.width = width;
		this.height = height;
		this.scale = scale;
	}
	public String getTitle(){ return title; }
	public int getWidth(){ return width; }
	public int getHeight(){ return height; }
	public int getScale(){ return scale; }
}
