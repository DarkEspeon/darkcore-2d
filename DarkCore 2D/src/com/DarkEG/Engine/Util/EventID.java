package com.DarkEG.Engine.Util;

public final class EventID {
	public static final int KEYBOARD = 0xf000; //Keyboard Event
	public static final int KEYBIND = 0xf001; //Keybind Event
	public static final int RENDER = 0xf002; //Render Event
	public static final int XRENDEROFFSET = 0xf003; //X Render Offset Event
	public static final int FILE = 0xf004; //File Load Event
	public static final int LEVELLOADED = 0xf005; //Level Loaded Event
	public static final int SPRITESWITCH = 0xf006; //Sprite Switch Event
	public static final int LOADLEVEL = 0xf007; //Load Level Event
	public static final int LEVELTRANSITION = 0xf008; //Level Transition Event
	public static final int WINDOWSIZE = 0xf009; //Window Size Event
	public static final int WINDOWSCALE = 0xf00a; //Window Scale Event
	public static final int WINDOWTITLE = 0xf00b; //Window Title Event
	public static final int PLAYERMOVE = 0xf00c; //Player Move Event
	public static final int MOUSEPRESS = 0xf00d; //Mouse Press Event
	public static final int MOUSERELEASE = 0xf00e; //Mouse Release Event
	public static final int MOUSEDRAG = 0xf00f; //Mouse Drag Event
	public static final int YRENDEROFFSET = 0xf010; //Y Render Offset Event

	public static final int DEAD = 0xffff; //Dead Event
}
