package com.DarkEG.Engine.Util;

import com.DarkEG.Engine.Input.KeybindManager;
import com.google.gson.annotations.Expose;

public class Setting {
	@Expose private WindowSettings view;
	@Expose private String tileFile;
	@Expose private String startingLevel;
	@Expose private String entityFile;
	@Expose private KeybindManager kbm;
	public Setting(WindowSettings view, String tileFile, String startingLevel, String entityFile, KeybindManager kbm){
		this.view = view;
		this.tileFile = tileFile;
		this.startingLevel = startingLevel;
		this.entityFile = entityFile;
		this.kbm = kbm;
	}
	public Setting(String title, int width, int height, int scale, String tileFile, String startingLevel, String entityFile, KeybindManager kbm){
		this.view = new WindowSettings(title, width, height, scale);
		this.tileFile = tileFile;
		this.startingLevel = startingLevel;
		this.entityFile = entityFile;
		this.kbm = kbm;
	}
	public WindowSettings getWindowSettings(){ return view; }
	public String getTileFile(){ return tileFile; }
	public String getStartingLevel(){ return startingLevel; }
	public String getEntityFile(){ return entityFile; }
}
