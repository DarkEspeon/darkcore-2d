package com.DarkEG.Engine.Util.Json;

import java.lang.reflect.Type;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.FileEvent;
import com.DarkEG.Engine.Event.Default.LoadLevelEvent;
import com.DarkEG.Engine.Input.KeybindManager;
import com.DarkEG.Engine.Util.Setting;
import com.DarkEG.Engine.Util.WindowSettings;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class SettingDeserializer implements JsonDeserializer<Setting>{
	public Setting deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		JsonObject obj = arg0.getAsJsonObject();
		WindowSettings ws = arg2.deserialize(obj.get("view"), WindowSettings.class);
		KeybindManager kbm = arg2.deserialize(obj.get("kbm"), KeybindManager.class);
		String tileFile = obj.get("tileFile").getAsString();
		String startingLevel = obj.get("startingLevel").getAsString();
		String entityFile = obj.get("entityFile").getAsString();
		Setting s = new Setting(ws, tileFile, startingLevel, entityFile, kbm);
		LoadLevelEvent lle = new LoadLevelEvent(startingLevel);
		FileEvent tfe = new FileEvent("Tile", tileFile);
		FileEvent efe = new FileEvent("EntityMaster", entityFile);
		EventCore.getBus().triggerEvent(tfe);
		EventCore.getBus().triggerEvent(lle);
		EventCore.getBus().triggerEvent(efe);
		return s;
	}
	
}
