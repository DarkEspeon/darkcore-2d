package com.DarkEG.Engine.Util.Json;

import java.lang.reflect.Type;

import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Event.Default.WindowTitleEvent;
import com.DarkEG.Engine.Util.WindowSettings;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class WindowSettingDeserializer implements JsonDeserializer<WindowSettings> {

	public WindowSettings deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		JsonObject obj = arg0.getAsJsonObject();
		String title = obj.get("title").getAsString();
		int width = obj.get("width").getAsInt();
		int height = obj.get("height").getAsInt();
		int scale = obj.get("scale").getAsInt();
		WindowSettings s = new WindowSettings(title, width, height, scale);
		WindowSizeEvent wsizee = new WindowSizeEvent(width, height);
		WindowScaleEvent wscalee = new WindowScaleEvent(scale);
		WindowTitleEvent wte = new WindowTitleEvent(title);
		EventCore.getBus().triggerEvent(wsizee);
		EventCore.getBus().triggerEvent(wscalee);
		EventCore.getBus().triggerEvent(wte);
		return s;
	}
	
}
