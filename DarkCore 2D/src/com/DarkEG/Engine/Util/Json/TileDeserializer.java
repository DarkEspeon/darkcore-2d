package com.DarkEG.Engine.Util.Json;

import java.lang.reflect.Type;

import com.DarkEG.Engine.Level.Tiles.AnimatedTile;
import com.DarkEG.Engine.Level.Tiles.BasicSolidTile;
import com.DarkEG.Engine.Level.Tiles.BasicTile;
import com.DarkEG.Engine.Level.Tiles.Tile;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class TileDeserializer implements JsonDeserializer<Tile[]>{
	//All 5 of following are present in all tile declarations, AnimationFrames and AnimationDelay are only present in animated tiles
	//Also, if Solid = true, register as a basic solid tile, not a basic tile
	//tileX, tileY, id, solid, color
	public Tile[] deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		JsonArray arr = arg0.getAsJsonArray();
		Tile[] res = new Tile[arr.size()];
		for(int i = 0; i < arr.size(); i++){
			if(arr.get(i).isJsonNull()) continue;
			JsonObject obj = arr.get(i).getAsJsonObject();
			
			int tileX = obj.get("tileX").getAsInt();
			int tileY = obj.get("tileY").getAsInt();
			int id = obj.get("id").getAsInt();
			boolean solid = obj.get("solid").getAsBoolean();
			int color = obj.get("color").getAsInt();
			Tile temp = null;
			if(obj.has("animationCoords")){
				int[][] animCoords = arg2.deserialize(obj.get("animationCoords"), int[][].class);
				int animDelay = obj.get("animationDelay").getAsInt();
				temp = new AnimatedTile(id, animCoords, color, animDelay);
				//animated tile
			} else if(solid) {
				temp = new BasicSolidTile(id, tileX, tileY, color);
				//basic solid tile
			} else {
				temp = new BasicTile(id, tileX, tileY, color);
				//basic tile
			}
			res[i] = temp;
		}
		return res;
	}
}
