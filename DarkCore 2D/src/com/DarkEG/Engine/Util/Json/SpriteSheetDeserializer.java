package com.DarkEG.Engine.Util.Json;

import java.lang.reflect.Type;

import com.DarkEG.Engine.Render.SpriteSheet;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class SpriteSheetDeserializer implements JsonDeserializer<SpriteSheet> {
	public SpriteSheet deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		JsonObject obj = arg0.getAsJsonObject();
		String name = obj.get("sheetName").getAsString();
		String path = obj.get("path").getAsString();
		int spriteSize = obj.get("spriteSize").getAsInt();
		return SpriteSheet.getSheet(path, name, spriteSize);
	}

}
