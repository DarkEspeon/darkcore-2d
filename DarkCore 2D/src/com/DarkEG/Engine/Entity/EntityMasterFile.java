package com.DarkEG.Engine.Entity;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class EntityMasterFile {
	@Expose private Map<String, String> entityMap;
	public EntityMasterFile(){
		entityMap = new HashMap<>();
	}
	public void addFile(String name, String file){
		this.entityMap.put(name, file);
	}
	public String getFile(String name){ return entityMap.get(name); }
	public boolean hasFile(String name){ return entityMap.containsKey(name); }
}
