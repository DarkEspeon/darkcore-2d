package com.DarkEG.Engine.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.DarkEG.Engine.Entity.Component.Factories.ComponentFactory;
import com.DarkEG.Engine.Entity.System.ComponentSystem;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.FileEvent;

public final class EntitySystem {
	private static EntitySystem entitySystem;
	
	public static void createSystem(){ entitySystem = new EntitySystem(); }
	public static EntitySystem getSystem(){ return entitySystem; }
	
	private List<ComponentSystem> system = new ArrayList<>();
	private Map<String, Entity> entities = new HashMap<>();
	private Map<String, EntityTemplate> templates = new HashMap<>();
	private Map<String, ComponentFactory> componentFactories = new HashMap<>();
	private EntityMasterFile emf;
	
	private EntitySystem(){}
	
	public Entity addEntity(String name){ Entity e = new Entity(name); entities.put(name, e); return e; }
	public Map<String, Entity> getEntities(){ return entities; }
	public void removeEntity(String name){ entities.remove(name); }
	public Entity getEntity(String name){ return entities.get(name); }
	
	public void addEntityTemplate(String name, EntityTemplate template){ templates.put(name, template); }
	public EntityTemplate getEntityTemplate(String name){ 
		if(!templates.containsKey(name)){
			if(emf.hasFile(name)){
				FileEvent fe = new FileEvent("Entity", emf.getFile(name));
				EventCore.getBus().triggerEvent(fe);
			}
		}
		return templates.get(name);
	}
	
	public void addComponentFactory(String name, ComponentFactory cf){ componentFactories.put(name, cf); }
	public ComponentFactory getFactory(String name){ return componentFactories.get(name); }
	
	public void addComponentSystem(ComponentSystem sys){ system.add(sys); }
	public List<ComponentSystem> getComponentSystems(){ return system; }
	
	public void createMasterFile(){ this.emf = new EntityMasterFile(); }
	public void setMasterFile(EntityMasterFile emf) { this.emf = emf; }
	public EntityMasterFile getMasterFile(){ return emf; }
}
