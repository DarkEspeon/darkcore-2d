package com.DarkEG.Engine.Entity.Component.Factories;

import java.util.Map;

import com.DarkEG.Engine.Entity.Component.Component;
import com.DarkEG.Engine.Util.Data;

public abstract class ComponentFactory<T extends Component> {
	public abstract T createComponent(Map<String, Data> variables);
}
