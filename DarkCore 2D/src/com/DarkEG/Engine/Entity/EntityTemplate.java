package com.DarkEG.Engine.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.DarkEG.Engine.Util.Data;
import com.google.gson.annotations.Expose;

public class EntityTemplate {
	@Expose private String templateName;
	@Expose private List<String> componentTypes;
	@Expose private Map<String, Data> defaultVariables;
	public EntityTemplate(String name, List<String> componentTypes, Map<String, Data> defaultVariables){
		this.templateName = name;
		this.componentTypes = new ArrayList<>();
		this.defaultVariables = new HashMap<>();
		this.componentTypes.addAll(componentTypes);
		this.defaultVariables.putAll(defaultVariables);
	}
	public Entity createEntity(String name, Map<String, Data> addedVariables){
		Map<String, Data> allVariables = new HashMap<>();
		allVariables.putAll(defaultVariables);
		for(String key : addedVariables.keySet()){
			allVariables.put(key, addedVariables.get(key));
		}
		Entity e = EntitySystem.getSystem().addEntity(name);
		for(String cType : componentTypes){
			e.addComponent(EntitySystem.getSystem().getFactory(cType).createComponent(allVariables));
		}
		return e;
	}
	public String getName(){ return templateName; }
}
