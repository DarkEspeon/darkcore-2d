package com.DarkEG.Engine.Parser;

import java.util.Map;

import org.w3c.dom.Element;

import com.DarkEG.Engine.Asset.XMLParser;
import com.DarkEG.Engine.Entity.EntityMasterFile;
import com.DarkEG.Engine.Entity.EntitySystem;

public class EntityFileXMLParser extends XMLParser{
	public void parse(String name, Element root){
		if(EntitySystem.getSystem().getMasterFile() == null) EntitySystem.getSystem().createMasterFile();
		EntityMasterFile emf = EntitySystem.getSystem().getMasterFile();
		for(Map<String, String> attribs : getList(root, "Entity", "Name", "File")){
			emf.addFile(attribs.get("Name"), attribs.get("File"));
		}
	}
}
