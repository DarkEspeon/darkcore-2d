package com.DarkEG.Engine.Parser;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import com.DarkEG.Engine.Game;
import com.DarkEG.Engine.Asset.XMLParser;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.FileEvent;
import com.DarkEG.Engine.Event.Default.LoadLevelEvent;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Event.Default.WindowTitleEvent;
import com.DarkEG.Engine.Input.Keybind;
import com.DarkEG.Engine.Input.KeybindManager;

public class SettingXMLParser extends XMLParser{
	private Game game;
	public SettingXMLParser(Game game){
		this.game = game;
	}
	public void parse(String file, Element rootElement) {
		List<Element> keyEle = getElements(rootElement, "Keybinds");
		for(Element e : keyEle){
			parseKeybind(e);
		}
		List<Map<String, String>> levels = getList(rootElement, "View", "Title", "Width", "Height", "Scale");
		for(Map<String, String> view : levels){
			parseView(view);
		}
		levels = getList(rootElement, "TileStore", "File");
		for(Map<String, String> level : levels){
			parseTileStore(level);
		}
		levels = getList(rootElement, "Level", "StartingLevel");
		for(Map<String, String> level : levels){
			parseLevel(level);
		}
		levels = getList(rootElement, "EntityStore", "File");
		for(Map<String, String> level : levels){
			parseEntityStore(level);
		}
	}
	private void parseKeybind(Element keybind){
		KeybindManager km = KeybindManager.getKeybinds();
		for(Map<String, String> attribs : getList(keybind, "Keybind", "Name", "Key", "CTRL", "SHIFT", "ALT", "Cooldown")){
			boolean ctrl = !"".equals(attribs.get("CTRL"));
			boolean shift = !"".equals(attribs.get("SHIFT"));
			boolean alt = !"".equals(attribs.get("ALT"));
			int key = KeybindManager.keyToInt.get(attribs.get("Key"));
			String name = attribs.get("Name");
			if("".equals(attribs.get("Cooldown"))) attribs.put("Cooldown", "0");
			long cooldown = Long.valueOf(attribs.get("Cooldown"));
			km.registerKeybind(new Keybind(name, key, ctrl, shift, alt, cooldown));
		}
	}
	private void parseView(Map<String, String> view){
		int height = parseInt(view.get("Height"));
		int width = parseInt(view.get("Width"));
		int scale = parseInt(view.get("Scale"));
		String title = view.get("Title");
		WindowSizeEvent wsizee = new WindowSizeEvent(width, height);
		WindowScaleEvent wscalee = new WindowScaleEvent(scale);
		WindowTitleEvent wte = new WindowTitleEvent(title);
		EventCore.getBus().triggerEvent(wsizee);
		EventCore.getBus().triggerEvent(wscalee);
		EventCore.getBus().triggerEvent(wte);
	}
	private void parseLevel(Map<String, String> level){
		String levelName = level.get("StartingLevel");
		LoadLevelEvent lle = new LoadLevelEvent(levelName);
		EventCore.getBus().triggerEvent(lle);
	}
	private void parseTileStore(Map<String, String> tileStore){
		FileEvent e = new FileEvent("Tile", tileStore.get("File"));
		EventCore.getBus().triggerEvent(e);
	}
	private void parseEntityStore(Map<String, String> entityStore){
		FileEvent e = new FileEvent("EntityMaster", entityStore.get("File"));
		EventCore.getBus().triggerEvent(e);
	}
}
