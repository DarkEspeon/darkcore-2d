package com.DarkEG.Engine.Parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import com.DarkEG.Engine.Asset.XMLParser;
import com.DarkEG.Engine.Entity.EntitySystem;
import com.DarkEG.Engine.Entity.EntityTemplate;
import com.DarkEG.Engine.Util.Data;


public class EntityTemplateXMLParser extends XMLParser{
	private List<String> componentTypes = new ArrayList<>();
	private Map<String, Data> variables = new HashMap<>();
	public void parse(String file, Element rootElement) {
		componentTypes.clear();
		variables.clear();
		String name = "";
		for(Map<String, String> attribs : getList(rootElement, "Name", "Name")){
			name = attribs.get("Name");
		}
		List<Element> elements = getElements(rootElement, "Components");
		for(Element element : elements){
			parseComponents(element);
		}
		EntityTemplate et = new EntityTemplate(name, componentTypes, variables);
		EntitySystem.getSystem().addEntityTemplate(name, et);
	}
	private void parseComponents(Element components){
		List<Element> elements = getElements(components, "Component");
		List<Map<String, String>> attribs = getList(components, "Component", "Type");
		for(int i = 0; i < elements.size() && i < attribs.size(); i++){
			componentTypes.add(attribs.get(i).get("Type"));
			parseComponent(elements.get(i));
		}
	}
	private void parseComponent(Element component){
		for(Map<String, String> attribs : getList(component, "Input", "Name", "Type", "Value")){
			if("".equals(attribs.get("Value"))) continue;
			if("Int".equalsIgnoreCase(attribs.get("Type"))){
				int dat = Integer.parseInt(attribs.get("Value"));
				Data<Integer> data = new Data<>(dat);
				variables.put(attribs.get("Name"), data);
			} else if("Float".equalsIgnoreCase(attribs.get("Type"))){
				float flo = Float.parseFloat(attribs.get("Value"));
				Data<Float> data = new Data<>(flo);
				variables.put(attribs.get("Name"), data);
			} else if("LongArray".equalsIgnoreCase(attribs.get("Type"))){
				long[] arr = parseLongArray(attribs.get("Value"));
				Data<long[]> data = new Data<>(arr);
				variables.put(attribs.get("Name"), data);
			}
		}
	}
	private long[] parseLongArray(String value){
		String[] temp = value.split(",");
		long[] ret = new long[temp.length];
		for(int i = 0; i < temp.length; i++){
			ret[i] = parseLong(temp[i]);
		}
		return ret;
	}
}