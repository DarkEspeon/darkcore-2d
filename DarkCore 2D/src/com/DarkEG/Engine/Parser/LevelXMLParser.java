package com.DarkEG.Engine.Parser;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import com.DarkEG.Engine.Asset.AssetLoader;
import com.DarkEG.Engine.Asset.XMLParser;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.Default.LevelLoadedEvent;
import com.DarkEG.Engine.Event.Default.SpriteSwitchEvent;
import com.DarkEG.Engine.Level.Level;
import com.DarkEG.Engine.Level.LevelConnection;
import com.DarkEG.Engine.Level.Tiles.Tile;
import com.DarkEG.Engine.Render.SpriteSheet;

public class LevelXMLParser extends XMLParser {
	public static boolean firstLoad = false;
	private List<LevelConnection> connections;
	public void parse(String file, Element rootElement) {
		connections = new ArrayList<>();
		List<Map<String, String>> attribs = getList(rootElement, "Name", "Name", "Readable");
		String name = attribs.get(0).get("Name");
		String readable = attribs.get(0).get("Readable");
		
		attribs = getList(rootElement, "Size", "Width", "Height");
		String widthStr = attribs.get(0).get("Width");
		String heightStr = attribs.get(0).get("Height");
		int width = parseInt(widthStr);
		int height = parseInt(heightStr);
		
		attribs = getList(rootElement, "SpriteSheet", "File", "SheetName", "SpriteSize");
		String sizeStr = attribs.get(0).get("SpriteSize");
		int size = parseInt(sizeStr);
		SpriteSheet sheet = SpriteSheet.getSheet(attribs.get(0).get("File"), attribs.get(0).get("SheetName"), size);
		
		List<Element> elements = getElements(rootElement, "TileMap");
		byte[] tiles = parseTileMap(file, name, elements.get(0));
		String tileMap = elements.get(0).getAttribute("File");
		
		elements = getElements(rootElement, "Connections");
		parseConnections(elements.get(0));
		
		Level l = new Level(name, readable, tileMap, width, height, sheet, tiles);
		for(LevelConnection lc : connections){
			l.addConnection(lc);
		}
		
		SpriteSwitchEvent sse = new SpriteSwitchEvent("TILES", sheet);
		LevelLoadedEvent lle = new LevelLoadedEvent(l);
		if(!firstLoad){
			firstLoad = true;
			EventCore.getBus().triggerEvent(lle);
		} else {
			EventCore.getBus().queueEvent(sse);
			EventCore.getBus().queueEvent(lle);
		}
	}
	private byte[] parseTileMap(String masterfile, String levelName, Element spriteSheet){
		String file = spriteSheet.getAttribute("File");
		BufferedImage im = AssetLoader.getLoader().getImage("/data/Levels/" + levelName + file);
		int[] tileColors = im.getRGB(0, 0, im.getWidth(), im.getHeight(), null, 0, im.getWidth());
		byte[] tiles = new byte[im.getWidth() * im.getHeight()];
		for(int y = 0; y < im.getHeight(); y++){
			for(int x = 0; x < im.getWidth(); x++){
				boolean tileFound = false;
				for(Tile t : Tile.tiles){
					if(tileFound) continue;
					if(t != null && t.getColor() == tileColors[x + y * im.getWidth()]){
						tiles[x + y * im.getWidth()] = t.getID();
						tileFound = true;
					}
				}
				if(!tileFound){
					throw new RuntimeException("COULD NOT LOAD LEVEL " + masterfile);
				}
			}
		}
		return tiles;
	}
	private void parseConnections(Element connections){
		List<Map<String, String>> attribs = getList(connections, "Connection", "Level", "FromX", "FromY", "ToX", "ToY");
		for(Map<String, String> attrib : attribs){
			String level = attrib.get("Level");
			int fromX = parseInt(attrib.get("FromX"));
			int fromY = parseInt(attrib.get("FromY"));
			int toX = parseInt(attrib.get("ToX"));
			int toY = parseInt(attrib.get("ToY"));
			LevelConnection lc = new LevelConnection(level, fromX, fromY, toX, toY);
			this.connections.add(lc);
		}
	}
	private void parseLevelEvents(Element levelEvent){
		
	}
}
