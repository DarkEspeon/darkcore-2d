package com.DarkEG.Engine.Parser;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import com.DarkEG.Engine.Asset.XMLParser;
import com.DarkEG.Engine.Level.Tiles.AnimatedTile;
import com.DarkEG.Engine.Level.Tiles.BasicSolidTile;
import com.DarkEG.Engine.Level.Tiles.BasicTile;

public class TileXMLParser extends XMLParser {
	public void parse(String file, Element rootElement) {
		List<Map<String, String>> attribs = getList(rootElement, "Spritesheet", "File");
		String spriteFile = attribs.get(0).get("File");
		
		//Tell game about spritefile
		
		attribs = getList(rootElement, "Tile", "Name", "Type", "ID");
		List<Element> tiles = getElements(rootElement, "Tile");
		for(int i = 0; i < attribs.size() && i < tiles.size(); i++){
			Map<String, String> attrib = attribs.get(i);
			Element tile = tiles.get(i);
			
			int id = Integer.parseInt(attrib.get("ID"));
			String name = attrib.get("Name");
			
			if("Basic".equalsIgnoreCase(attrib.get("Type"))){
				parseBasic(tile, id, name);
			} else if("BasicSolid".equalsIgnoreCase(attrib.get("Type"))){
				parseSolid(tile, id, name);
			} else if("Animated".equalsIgnoreCase(attrib.get("Type"))){
				parseAnimated(tile, id, name);
			}
		}
	}
	private void parseBasic(Element basic, int id, String name){
		//Sprite
		List<Map<String, String>> attribs = getList(basic, "Sprite", "X", "Y");
		String xStr = attribs.get(0).get("X");
		String yStr = attribs.get(0).get("Y");
		int x = Integer.parseInt(xStr);
		int y = Integer.parseInt(yStr);
		//Color
		attribs = getList(basic, "Color", "Color");
		String color = attribs.get(0).get("Color");
		long colorLong = parseLong(color);
		int colorInt = (int) colorLong;
		//new BasicTile(ID, X, Y, Color)
		new BasicTile(id, x, y, colorInt);
	}
	private void parseSolid(Element solid, int id, String name){
		//Sprite
		List<Map<String, String>> attribs = getList(solid, "Sprite", "X", "Y");
		String xStr = attribs.get(0).get("X");
		String yStr = attribs.get(0).get("Y");
		int x = Integer.parseInt(xStr);
		int y = Integer.parseInt(yStr);
		//Color
		attribs = getList(solid, "Color", "Color");
		String color = attribs.get(0).get("Color");
		long colorLong = parseLong(color);
		int colorInt = (int) colorLong;
		//new BasicSolidTile(ID, X, Y, Color)
		new BasicSolidTile(id, x, y, colorInt);
	}
	private void parseAnimated(Element animated, int id, String name){
		//Animated Sprite
		List<Element> elems = getElements(animated, "AnimatedSprite");
		Element animSprite = elems.get(0);
		int[][] animCoords = parseAnimatedSprite(animSprite);
		//Color
		List<Map<String, String>> attribs = getList(animated, "Color", "Color");
		String color = attribs.get(0).get("Color");
		long colorLong = parseLong(color);
		int colorInt = (int) colorLong;
		//Animation Delay
		attribs = getList(animated, "Animation", "Delay");
		String delayStr = attribs.get(0).get("Delay");
		int delayInt = Integer.parseInt(delayStr);
		//new AnimatedTile(ID, AnimCoords[][], Color, Delay)
		new AnimatedTile(id, animCoords, colorInt, delayInt);
	}
	private int[][] parseAnimatedSprite(Element animSprite){
		List<Map<String, String>> attribs = getList(animSprite, "Frame", "X", "Y");
		int[][] ret = new int[attribs.size()][2];
		for(int i = 0; i < attribs.size(); i++){
			Map<String, String> attrib = attribs.get(i);
			String xStr = attrib.get("X");
			String yStr = attrib.get("Y");
			int x = Integer.parseInt(xStr);
			int y = Integer.parseInt(yStr);
			ret[i][0] = x;
			ret[i][1] = y;
		}
		return ret;
	}
}
