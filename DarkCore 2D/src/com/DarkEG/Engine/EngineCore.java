package com.DarkEG.Engine;

import java.io.File;
import java.net.URLDecoder;

import com.DarkEG.Engine.Asset.AssetLoader;
import com.DarkEG.Engine.Asset.FilesystemAssetPath;
import com.DarkEG.Engine.Asset.XMLLoader;
import com.DarkEG.Engine.Entity.EntityMasterFile;
import com.DarkEG.Engine.Entity.EntitySystem;
import com.DarkEG.Engine.Entity.EntityTemplate;
import com.DarkEG.Engine.Event.Event;
import com.DarkEG.Engine.Event.EventCore;
import com.DarkEG.Engine.Event.EventListener;
import com.DarkEG.Engine.Event.Default.FileEvent;
import com.DarkEG.Engine.Event.Default.LevelLoadedEvent;
import com.DarkEG.Engine.Event.Default.LevelTransitionEvent;
import com.DarkEG.Engine.Event.Default.LoadLevelEvent;
import com.DarkEG.Engine.Event.Default.PlayerMoveEvent;
import com.DarkEG.Engine.Event.Default.WindowScaleEvent;
import com.DarkEG.Engine.Event.Default.WindowSizeEvent;
import com.DarkEG.Engine.Event.Default.WindowTitleEvent;
import com.DarkEG.Engine.Event.Default.XOffsetEvent;
import com.DarkEG.Engine.Event.Default.YOffsetEvent;
import com.DarkEG.Engine.Input.KeybindManager;
import com.DarkEG.Engine.Level.Level;
import com.DarkEG.Engine.Level.Tiles.Tile;
import com.DarkEG.Engine.Process.ProcessManager;
import com.DarkEG.Engine.Util.EventID;
import com.google.gson.Gson;


public class EngineCore implements Runnable, EventListener {
	private GameWindow window;
	private GameCanvas canvas;
	private ProcessManager pm;
	private Game game;
	private boolean running = false;
	private boolean headless = false;
	public int width = 0, height = 0, scale = 0;
	private Level level;
	public String title = "";
	private String JarPath;
	private boolean DEV = false;
	private boolean visible = false;

	public EngineCore(Game game){
		this(game, false);
	}
	public EngineCore(Game game, boolean DEV){
		this(game, DEV, true);
	}
	public EngineCore(Game game, boolean DEV, boolean visible){
		this.DEV = DEV;
		this.visible = visible;
		this.game = game;
		this.headless = game.isHeadless();
		EventCore.createBus(2);
		KeybindManager.createManager();
		pm = new ProcessManager();
		AssetLoader.initLoader();
		XMLLoader.initXMLLoader();
	}
	public void dataInit(){
		window = new GameWindow();
		canvas = new GameCanvas();
		EventCore.getBus().addListener(this, EventID.FILE);
		EventCore.getBus().addListener(this, EventID.LOADLEVEL);
		EventCore.getBus().addListener(this, EventID.LEVELTRANSITION);
		EventCore.getBus().addListener(this, EventID.WINDOWSIZE);
		EventCore.getBus().addListener(this, EventID.WINDOWSCALE);
		EventCore.getBus().addListener(this, EventID.WINDOWTITLE);
		EventCore.getBus().addListener(this, EventID.LEVELLOADED);
		EventCore.getBus().addListener(this, EventID.PLAYERMOVE);
		try{
			JarPath = URLDecoder.decode(game.getClass().getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8");
			JarPath = JarPath.substring(0, JarPath.lastIndexOf("/") + 1);
		} catch (Exception e){
			e.printStackTrace();
		}
		if(DEV){
			AssetLoader.getLoader().addAssetPath(new FilesystemAssetPath(new File(JarPath)));
			AssetLoader.getLoader().addAssetPath(new FilesystemAssetPath(new File("res")));
		} else {
			AssetLoader.getLoader().addAssetPath(new FilesystemAssetPath(new File(JarPath + "res")));
		}
		EntitySystem.createSystem();
		game.dataInit(pm, this, DEV);
	}
	public void graphicsInit(){
		window.init(visible);
		canvas.init(visible);
		window.add(canvas);
		game.graphicsInit(window, canvas);
	}
	public synchronized void start(){
		running = true;
		dataInit();
		graphicsInit();
		new Thread(this, "Game").start();
	}
	public synchronized void stop(){
		running = false;
	}
	public void run(){
		int ticks = 0;
		int frames = 0;
		long lastTimer = System.currentTimeMillis();
		long lastTick = System.nanoTime();
		while(running){
			float delta = System.nanoTime() - lastTick;
			delta /= 1000000;
			lastTick = System.nanoTime();
			tick(delta);
			ticks++;
			if(!headless){
				frames++;
				render();
			}
			if(System.currentTimeMillis() - lastTimer > 1000){
				lastTimer += 1000;
				WindowTitleEvent wte = new WindowTitleEvent("FPS: " + frames + ", UPS: " + ticks);
				EventCore.getBus().queueEvent(wte);
				ticks = 0;
				frames = 0;
			}
		}
	}
	public void openWindow(){
		window.setVisible(true);
		canvas.setVisible(true);
	}
	public void closeWindow(){
		window.setVisible(false);
		canvas.setVisible(false);
	}
	public void render(){
		canvas.startFrame();
		game.onRender();
		canvas.render(game.getScreenPixels());
		canvas.endFrame();
	}
	public void tick(float delta){
		game.tick(delta);
	}
	public GameCanvas getCanvas(){
		return canvas;
	}
	public GameWindow getWindow(){
		return window;
	}
	public boolean handle(Event e){
		if(e instanceof FileEvent){
			AssetLoader load = AssetLoader.getLoader();
			Gson gson = load.getGsonInstance();
			FileEvent fe = (FileEvent) e;
			switch(fe.getName()){
				case "Tile":
					Tile.tiles = gson.fromJson(load.getFileContents(fe.getFilePath()), Tile[].class);
					return true;
				case "EntityMaster":
					EntityMasterFile emf = gson.fromJson(load.getFileContents(fe.getFilePath()), EntityMasterFile.class);
					EntitySystem.getSystem().setMasterFile(emf);
					return true;
				case "Entity":
					EntityTemplate et = gson.fromJson(load.getFileContents(fe.getFilePath()), EntityTemplate.class);
					EntitySystem.getSystem().addEntityTemplate(et.getName(), et);
					return true;
				default:
					return false;
			}
		}
		if(e instanceof LoadLevelEvent){
			LoadLevelEvent lle = (LoadLevelEvent) e;
			Level.loadLevel(lle.getLevelName());
			return true;
		}
		if(e instanceof LevelTransitionEvent){
			LevelTransitionEvent lte = (LevelTransitionEvent) e;
			LoadLevelEvent lle = new LoadLevelEvent(lte.getLevel());
			EventCore.getBus().triggerEvent(lle);
			return true;
		}
		if(e instanceof WindowSizeEvent){
			WindowSizeEvent wse = (WindowSizeEvent) e;
			this.width = wse.getWidth();
			this.height = wse.getHeight();
			return true;
		}
		if(e instanceof WindowScaleEvent){
			WindowScaleEvent wse = (WindowScaleEvent) e;
			this.scale = wse.getScale();
			return true;
		}
		if(e instanceof WindowTitleEvent){
			WindowTitleEvent wte = (WindowTitleEvent) e;
			this.title = wte.getTitle();
			return true;
		}if(e instanceof LevelLoadedEvent){
			LevelLoadedEvent lle = (LevelLoadedEvent) e;
			this.level = lle.getLevel();
			return true;
		}if(e instanceof PlayerMoveEvent){
			PlayerMoveEvent pme = (PlayerMoveEvent) e;
			int x = pme.getX();
			int y = pme.getY();
			int xOffset = ((int) x) - (width / 2);
			int yOffset = ((int) y) - (height / 2);

			if(xOffset < 0) xOffset = 0;
			if(xOffset > ((level.getWidth() * 8) - width)) xOffset = ((level.getWidth() * 8) - width);
			if(yOffset < 0) yOffset = 0;
			if(yOffset > ((level.getHeight() * 8) - height)) yOffset = ((level.getHeight() * 8) - height); 
			
			EventCore.getBus().triggerEvent(new XOffsetEvent(xOffset));
			EventCore.getBus().triggerEvent(new YOffsetEvent(yOffset));
			return true;
		}
		return false;
	}
}
